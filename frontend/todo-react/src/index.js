import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter, Switch, Route,  } from 'react-router-dom';
import Login from './components/login/login';
import Home from './components/home/home';
import Register from './components/login/register/register';

ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Route path="/login" component={Login}/>
            <Route path="/register" component={Register}/>
            <Route path="/" component={Home}/>
        </Switch>
    </BrowserRouter>
, document.getElementById('root'));
registerServiceWorker();