import { CardsRepository } from './cards.repository';

export class CardsService {
    constructor() {
        this._repository = new CardsRepository();
    }

    list() {
        const user = JSON.parse(sessionStorage.getItem('user'))._id;
        return this._repository.list(user);
    }

    delete(_id) {
        return this._repository.delete(_id);
    }

    update(note) {
        return this._repository.update(note);
    }

    insert(note) {
        return this._repository.insert(note);
    }
}