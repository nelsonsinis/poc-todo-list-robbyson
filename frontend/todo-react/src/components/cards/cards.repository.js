import { request } from 'requestify';
import api from '../../App.api';

export class CardsRepository {
    constructor() {
        this.list = this.list.bind(this);
        this.delete = this.delete.bind(this);
        this.update = this.update.bind(this);
    }

    list(user) {
        return new Promise(async (resolve, reject) => {
            try {
                const response = await request(`${api.serviceNote}/?userId=${user}`, {
                    method: 'GET',
                    headers: {
                        'x-access-token': sessionStorage.getItem('token')
                    }
                });

                const body = response.getBody();
                resolve(body);
            } catch(error) {
                reject(error);
            }
        });
    }

    delete(_id) {
        return new Promise(async (resolve, reject) => {
            try {
                await request(`${api.serviceNote}/${_id}`, {
                    method: 'DELETE',
                    headers: {
                        'x-access-token': sessionStorage.getItem('token')
                    }
                });

                resolve();
            } catch(error) {
                reject(error);
            }
        });
    }

    update(note) {
        return new Promise(async (resolve, reject) => {
            try {
                const response = await request(api.serviceNote, {
                    method: 'PUT',
                    headers: {
                        'x-access-token': sessionStorage.getItem('token')
                    },
                    body: note
                });
                const body = response.getBody();
                resolve(body);
            } catch(error) {
                reject(error);
            }
        });
    }

    insert(note) {
        return new Promise(async (resolve, reject) => {
            try {
                const response = await request(`${api.serviceNote}/insert`, {
                    method: 'PUT',
                    headers: {
                        'x-access-token': sessionStorage.getItem('token')
                    },
                    body: note
                });

                const body = response.getBody();
                resolve(body);
            } catch(error) {
                reject(error);
            }
        });
    }
}