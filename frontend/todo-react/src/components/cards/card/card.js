import React from 'react';
import './card.css';
import CardForm from '../card-edit/card-edit';

export default class Card extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            tasks: [],
            _id: ''
        }

        this.handleTask = this.handleTask.bind(this);
        this.deleteNote = this.deleteNote.bind(this);
        this.updateNote = this.updateNote.bind(this);
    }

    componentDidMount() {
        this.setState({
            title: this.props.card.title,
            tasks: this.props.card.tasks,
            _id: this.props.card._id
        });
    }

    componentWillReceiveProps() {
        this.setState({
            title: this.props.card.title,
            tasks: this.props.card.tasks,
            _id: this.props.card._id
        });
    }

    handleTask(index) {
        const tasks = this.state.tasks;
        tasks[index].completed = !tasks[index].completed;

        this.setState({
            tasks
        });

        this.props.updateNote({
            title: this.state.title,
            _id: this.state._id,
            userId: JSON.parse(sessionStorage.getItem('user'))._id,
            tasks
        })
    }

    deleteNote() {
        this.props.deleteNote(this.state._id);
    }

    updateNote(note) {
        this.setState({...note});
        this.props.updateNote(note);
    }

    render() {
        const tasks = this.state.tasks.map((task, index) => {
            return (
                <div key={index}>
                    <input type="checkbox" id={`${task._id}${index}`} checked={task.completed} onChange={() => this.handleTask(index)}/>
                    <label htmlFor={`${task._id}${index}`} style={{textDecoration: task.completed ? 'line-through' : 'none'}}>{task.description}</label>
                </div>
            )
        });

        const updateCard = {
            title: this.state.title,
            tasks: this.state.tasks,
            _id: this.state._id
        }

        const triggerForm = <i id="edit" className="small material-icons grey-text">edit</i>;

        return (
            <div className="col s12 m6 l6 xl6">
                <div className="card white">
                    <div className="card-content">
                        <form>
                            <div id="cardTitle">
                                <p className="card-title">{this.state.title}</p>
                                <div id="buttonsCard">
                                    <CardForm card={updateCard} trigger={triggerForm} updateNote={this.updateNote}/>
                                    <i id="delete" className="small material-icons red-text" onClick={this.deleteNote}>delete</i>
                                </div>
                            </div>
                            <div>
                                {tasks}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}