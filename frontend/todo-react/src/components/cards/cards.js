import React from 'react';
import { CardsService } from './cards.service';
import { LoginService } from '../login/login.service';
import { withRouter } from 'react-router-dom';
import Card from './card/card';
import CardForm from './card-edit/card-edit';
import './cards.css';

class Cards extends React.Component {
    state = {
        cards: []
    }

    constructor(props) {
        super(props);
        this.service = new CardsService();
        this.loginService = new LoginService();

        this.insertNote = this.insertNote.bind(this);
        this.updateNote = this.updateNote.bind(this);
        this.deleteNote = this.deleteNote.bind(this);
    }

    async componentDidMount() {
        try {
            const cards = await this.service.list();
            this.setState({
                cards: cards.items
            });
        } catch(error) {
            window.Materialize.toast(error.error || 'An error occurred', 4000);
        }
    }

    async insertNote(note) {
        try {
            note.userId = JSON.parse(sessionStorage.getItem('user'))._id;
            const response = await this.service.insert(note);

            this.setState({
                cards: [...this.state.cards, response.data]
            });
        } catch(error) {
            window.Materialize.toast(error.error || 'An error occurred', 4000);
        }
    }

    async updateNote(note) {
        try {
            await this.service.update(note);
            window.Materialize.toast('Note updated successfully', 4000);
        } catch(error) {
            window.Materialize.toast(error.error || 'An error occurred', 4000);
        }
    }
    
    async deleteNote(_id) {
        try {
            const cards = this.state.cards.filter(card => card._id !== _id);
            await this.service.delete(_id);
            this.setState({
                cards
            });
            window.Materialize.toast('Note deleted successfully', 2000);
        } catch(error) {
            window.Materialize.toast(error.error || 'An error occurred', 4000);
        }
    }

    render() {
        const cards = this.state.cards.map((card, index) => {
            return <Card card={card} key={index} updateNote={this.updateNote} deleteNote={this.deleteNote}/>
        });

        const newCardForm = {
            title: '',
            tasks: [
                {
                    completed: false,
                    description: ''
                }
            ],
            _id: ''
        }

        const triggerForm = <div id="newNote"><a className="btn-floating btn-large waves-effect waves-light blue lighten-1"><i className="material-icons">add</i></a></div>

        return (
            <div className="row">
                {cards}
                <CardForm card={newCardForm} trigger={triggerForm} insertNote={this.insertNote}/>
            </div>
        )
    }
}

export default withRouter(Cards);