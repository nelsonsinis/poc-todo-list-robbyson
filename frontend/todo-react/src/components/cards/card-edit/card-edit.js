import React from 'react';
import { Modal } from 'react-materialize';
import './card-edit.css';

export default class CardForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            tasks: [{
                completed: false,
                description: ''
            }],
            _id: ''
        }

        this.ready = this.ready.bind(this);
        this.complete = this.complete.bind(this);
        this.handleTitle = this.handleTitle.bind(this);
        this.handleTask = this.handleTask.bind(this);
        this.handleTaskDescription = this.handleTaskDescription.bind(this);
        this.addTask = this.addTask.bind(this);
        this.insertNote = this.insertNote.bind(this);
        this.updateNote = this.updateNote.bind(this);
        this.deleteTask = this.deleteTask.bind(this);
    }
    
    componentWillReceiveProps() {
        this.setState({
            title: this.props.card.title,
            tasks: this.props.card.tasks,
            _id: this.props.card._id
        });
    }
    
    ready() {
        this.setState({
            title: this.props.card.title,
            tasks: this.props.card.tasks || [
                {
                    completed: false,
                    description: ''
                }
            ],
            _id: this.props.card._id
        });
    }

    complete() {
        this.setState({
            title: '',
            tasks: [{
                description: '',
                completed: false
            }],
            _id: ''
        });
    }

    handleTitle(event) {
        this.setState({
            title: event.target.value
        })
    }

    handleTask(index) {
        const tasks = this.state.tasks;
        tasks[index].completed = !tasks[index].completed;

        this.setState({
            tasks
        });
    }

    handleTaskDescription(index, value) {
        const tasks = this.state.tasks;
        tasks[index].description = value;

        this.setState({
            tasks
        });
    }

    addTask() {
        this.setState({
            tasks: [...this.state.tasks, {completed: false, description: ''}]
        })
    }

    insertNote() {
        const note = {
            title: this.state.title,
            tasks: this.state.tasks
        };

        this.props.insertNote(note);
    }

    updateNote() {
        const note = {
            title: this.state.title,
            tasks: this.state.tasks,
            _id: this.state._id,
            userId: JSON.parse(sessionStorage.getItem('user'))._id
        }

        this.props.updateNote(note);
    }

    deleteTask(index) {
        const tasks = this.state.tasks;
        tasks.splice(index, 1);
        this.setState({
            tasks
        });
    }

    render() {
        const tasks = this.state.tasks.map((task, index) => {
            return (
                <div id="tasks" key={index}>
                    <div id="tasksForm">
                        <div>
                            <input type="checkbox" id={`completed${index}${task._id || ''}`} checked={task.completed} onChange={() => this.handleTask(index)}/>
                            <label htmlFor={`completed${index}${task._id || ''}`}>{task.completed}</label>
                        </div>
                        <div className="input-field">
                            <input type="text" id={`description${index}`} value={task.description} onChange={(event) => this.handleTaskDescription(index, event.target.value)}/>
                            <label htmlFor={`description${index}`} className={task.description ? 'active' : ''}>Description</label>
                        </div>
                    </div>
                    <div onClick={this.deleteTask}>
                        <i className="material-icons red-text">delete</i>
                    </div>
                </div>
            )
        });

        return (
            <Modal
                header={this.state._id ? 'Update a note' : 'Insert a new note'}
                trigger={this.props.trigger}
                modalOptions={{ready: this.ready, complete: this.complete}}
                actions={
                    <div id="actions">
                        <button className="btn red lighten-1 modal-close">Cancel</button>
                        <button className="btn blue lighten-1 modal-close" onClick={this.props.insertNote ? this.insertNote : this.updateNote}>Save</button>
                    </div>
                }>
                <form>
                    <div className="input-field">
                        <input id="title" type="text" value={this.state.title} onChange={this.handleTitle}/>
                        <label className={this.state.title ? 'active' : ''} htmlFor="title">Title</label>
                    </div>
                    <div>
                        {tasks}
                        <div id="btnAddTasks">
                            <div className="btn blue lighten-1 waves-effect waves-light" onClick={this.addTask}>
                                <i className="material-icons">add</i>
                            </div>
                        </div>
                    </div>
                </form>
            </Modal>
        )
    }
}