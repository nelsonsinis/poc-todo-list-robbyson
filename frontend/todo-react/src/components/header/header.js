import React from 'react';
import './header.css';
import {
    Link,
    withRouter
} from 'react-router-dom';

class Header extends React.Component {
    constructor(props) {
        super(props);
        const token = sessionStorage.getItem('token');
        this.state = {
            route: token ? '/' : '/login'
        };

        this.getUser = this.getUser.bind(this);
        this.exit = this.exit.bind(this);
    }

    exit() {
        sessionStorage.clear();
        this.props.history.push('/login');
    }

    getUser() {
        if (this.props.user) {
            return (
                <div id="user" className="hide-on-med-and-down">
                    <div id="headerPhoto">
                        <img src={this.props.user.photo} alt=""/>
                    </div>
                    <div id="headerUsername">
                        {this.props.user.displayName}
                    </div>
                    <div className="red lighten-1" id="headerExit" onClick={this.exit}>
                        <i className="small material-icons">exit_to_app</i>
                    </div>
                </div>
            )
        }
    }

    render() {
        return (
            <nav>
                <div className="nav-wrapper light-blue lighten-1">
                    <div>
                        <Link to={this.state.route} className="brand-logo">
                            <i className="small material-icons">check_box</i>
                            Todo List
                        </Link>
                    </div>
                    {this.getUser()}
                </div>
            </nav>
        )
    }
}

export default withRouter(Header);