import { request } from 'requestify';
import api from '../../../App.api';

export class RegisterRepository {
    constructor() {
        this.register = this.register.bind(this);
    }

    register(user) {
        return new Promise(async (resolve, reject) => {
            try {
                const response = await request(`${api.serviceUser}/register`, {
                    method: 'POST',
                    body: user
                });
                const body = response.getBody();
                resolve(body);
            } catch(error) {
                reject(error);
            }
        });
    }
}