import React, { Component } from 'react';
import {
    withRouter
} from 'react-router-dom';
import Header from '../../header/header';
import './register.css';
import { RegisterService } from './register.service';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            phoneNumber: '',
            password: '',
            photo: '',
            displayName: '',
            showPhoto: false
        }

        this.handlePhoto = this.handlePhoto.bind(this);
        this.handleUsername = this.handleUsername.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        this.handleDisplayName = this.handleDisplayName.bind(this);
        this.handlePhoneNumber = this.handlePhoneNumber.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validForm = this.validForm.bind(this);
        this.service = new RegisterService();
    }

    handlePhoto(event) {
        let reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;

            reader.readAsDataURL(file);

            reader.onload = () => {
                this.setState({
                    photo: reader.result,
                    showPhoto: true
                })
            }
        }
    }

    handleUsername(event) {
        this.setState({
            username: event.target.value
        });
    }

    handlePassword(event) {
        this.setState({
            password: event.target.value
        });
    }

    handleDisplayName(event) {
        this.setState({
            displayName: event.target.value
        });
    }

    handlePhoneNumber(event) {
        this.setState({
            phoneNumber: event.target.value
        })
    }

    async handleSubmit(event) {
        event.preventDefault();
        try {
            const user = {
                username: this.state.username,
                password: this.state.password,
                displayName: this.state.displayName,
                photo: this.state.photo,
                phoneNumber: this.state.phoneNumber
            }

            await this.service.register(user);
            window.Materialize.toast('User created successfully', 2000);
            this.props.history.push('/login');
        } catch(error) {
            window.Materialize.toast(error.error || 'An error occurred', 4000);
        }
    }

    showPhoto() {
        if (this.state.showPhoto) {
            return (
                <div id="previewPhoto">
                    <img src={this.state.photo} alt=""/>
                </div>
            )
        }

        return <i className="large material-icons">add_a_photo</i>;
    }

    validForm() {
        return !this.state.username ||
        !this.state.password ||
        !this.state.phoneNumber ||
        !this.state.photo ||
        this.state.password.length < 6 ||
        !this.state.displayName
    }

    render() {
        return (
            <div>
                <Header/>
                <div className="container auto-height">
                    <form className="auto-height" id="register" onSubmit={this.handleSubmit}>
                        <div className="center">
                            <input type="file" id="photo" onChange={this.handlePhoto}/>
                            <label htmlFor="photo">
                                {this.showPhoto()}
                            </label>
                        </div>
                        <div className="input-field">
                            <input type="text" name="username" id="username" onChange={this.handleUsername}/>
                            <label htmlFor="username">Username</label>
                        </div>
                        <div className="input-field">
                            <input type="text" name="phoneNumber" id="phoneNumber" onChange={this.handlePhoneNumber}/>
                            <label htmlFor="phoneNumber">Phone Number</label>
                        </div>
                        <div className="input-field">
                            <input type="password" name="password" id="password" onChange={this.handlePassword}/>
                            <label htmlFor="password">Password</label>
                        </div>
                        <div className="input-field">
                            <input type="text" name="displayName" id="displayName" onChange={this.handleDisplayName}/>
                            <label htmlFor="displayName">Display Name</label>
                        </div>
                        <div className="center">
                            <input className="btn blue lighten-1" type="submit" value="Save" disabled={this.validForm()}/>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default withRouter(Register);