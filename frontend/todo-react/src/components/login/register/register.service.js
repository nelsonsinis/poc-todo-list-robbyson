import { RegisterRepository } from './register.repository';

export class RegisterService {
    constructor() {
        this._repository = new RegisterRepository();
        this.register = this.register.bind(this);
    }

    register(user) {
        return this._repository.register(user);
    }
}