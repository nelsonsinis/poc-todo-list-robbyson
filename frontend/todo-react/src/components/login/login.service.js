import { LoginRepository } from './login.repository';

export class LoginService {
    constructor() {
        this._repository = new LoginRepository();
        this.login = this.login.bind(this);
        this.validToken = this.validToken.bind(this);
    }

    login(username, password) {
        return this._repository.login(username, password);
    }

    validToken() {
        return this._repository.validToken();
    }
}