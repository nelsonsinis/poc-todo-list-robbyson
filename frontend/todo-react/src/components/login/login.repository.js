import { request } from 'requestify';
import api from '../../App.api';

export class LoginRepository {
    constructor() {
        this.login = this.login.bind(this);
    }

    login(username, password) {
        return new Promise(async (resolve, reject) => {
            try {
                const response = await request(api.serviceUser, {
                    method: 'POST',
                    body: {
                        username, password
                    }
                });
                const body = response.getBody();
                resolve(body);
            } catch(error) {
                reject(error);
            }
        });
    }

    validToken() {
        return new Promise(async (resolve, reject) => {
            try {
                const response = await request(api.serviceUser, {
                    method: 'GET',
                    headers: {
                        'x-access-token': sessionStorage.getItem('token')
                    }
                });

                const body = response.getBody();
                resolve(body);
            } catch(error) {
                reject(error);
            }
        });
    }
}