import React from 'react';
import './login.css';
import { withRouter } from 'react-router-dom';
import Header from '../header/header';
import { LoginService } from './login.service';
import {
    Link
} from 'react-router-dom';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        }
        this.service = new LoginService();

        if (sessionStorage.getItem('token')) {
            this.service.validToken().then(() => {
                this.props.history.push('/');
            });
        }


        this.handleUsername = this.handleUsername.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleUsername(event) {
        this.setState({
            username: event.target.value
        });
    }

    handlePassword(event) {
        this.setState({
            password: event.target.value
        });
    }

    async handleSubmit(event) {
        event.preventDefault();
        try {
            const response = await this.service.login(this.state.username, this.state.password);
            sessionStorage.setItem('token', response.data.token);
            sessionStorage.setItem('user', JSON.stringify(response.data.user));
            this.props.history.push('/');
        } catch(error) {
            window.Materialize.toast(error.error, 4000);
        }
    }

    render() {
        return (
            <div id="loginBody">
                <Header/>
                <div className="container" id="login">
                    <div className="row center">
                        <i className="large material-icons">person_pin</i>
                        <form onSubmit={this.handleSubmit}>
                            <div className="input-field">
                                <input type="text" value={this.state.username} onChange={this.handleUsername} id="username" autoComplete="off"/>
                                <label htmlFor="username">Login</label>
                            </div>
                            <div className="input-field">
                                <input type="password" value={this.state.password} onChange={this.handlePassword} id="password" autoComplete="off"/>
                                <label htmlFor="password">Password</label>
                            </div>
                            <div className="col s12 m12 l12 xl12">
                                <input className="btn blue lighten-1" type="submit" name="submit" value="Login" disabled={!this.state.password || !this.state.username}/>
                            </div>
                        </form>
                        <div className="col s12 m12 l12 xl12" id="btnRegister">
                            <Link className="btn margin-2 blue lighten-1" to="/register">Register</Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Login);