import React from 'react';
import Header from '../header/header';
import {
    withRouter
} from 'react-router-dom';
import { LoginService } from '../login/login.service';
import Cards from '../cards/cards';
import './home.css';

class Home extends React.Component {
    state = {
        user: {},
        isLogged: false
    }

    constructor(props) {
        super(props);
        this.loginService = new LoginService();
    }
    
    componentDidMount() {
        this.setState({
            user: JSON.parse(sessionStorage.getItem('user'))
        });
    }

    async componentWillMount() {
        if (sessionStorage.getItem('token')) {
            let isLogged;
    
            try {
                isLogged = (await this.loginService.validToken()).data;
                this.setState({
                    isLogged
                })
            } catch(error) {
                sessionStorage.clear();
                window.Materialize.toast('Your session expired', 2000);
                this.props.history.push('/login');
            }
        } else {
            this.props.history.push('/login');
        }
    }
    
    render() {
        if (this.state.isLogged) {
            return (
                <div>
                    <Header user={this.state.user}/>
                    <Cards />
                </div>
            )
        }

        return null;
    }
}

export default withRouter(Home)