import { NgModule } from "@angular/core";
import { ModalNoteComponent } from "./modal-note.component";
import { Routes, RouterModule } from "@angular/router";

const ROUTES: Routes = [
    {
        path: '',
        component: ModalNoteComponent
    }
]

@NgModule({
    declarations: [
        ModalNoteComponent
    ],
    imports: [
        RouterModule,
        RouterModule.forChild(ROUTES)
    ]
})
export class ModalNoteModule {}