import { Component, OnInit, Input, AfterContentInit, Output, EventEmitter, OnDestroy, AfterViewInit } from '@angular/core';
import { NotesService } from '../notes.service';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { NoteModel } from '../note/note.model';
import { toast } from 'angular2-materialize';
import { NotesRepository } from '../notes.repository';

@Component({
  selector: 'rb-modal-note',
  templateUrl: './modal-note.component.html',
  styleUrls: ['./modal-note.component.css'],
  providers: [
    NotesService,
    NotesRepository
  ]
})
export class ModalNoteComponent implements OnInit {
  note: NoteModel;
  form: FormGroup;
  @Output() saveEvent = new EventEmitter();

  private service: NotesService;
  private builder: FormBuilder;
  constructor(service: NotesService, builder: FormBuilder) {
    this.service = service;
    this.builder = builder;

    this.form = this.builder.group({
      title: this.builder.control('', Validators.required),
      tasks: this.builder.array([this.buildItem()], Validators.required)
    });
  }

  ngOnInit() {
  }

  buildItem(description: String = '', completed: Boolean = false) {
    return this.builder.group({
      completed: this.builder.control(completed, Validators.required),
      description: this.builder.control(description, Validators.required)
    })
  }

  addItem() {
    const control = this.form.controls['tasks'];
    (control as FormArray).push(this.buildItem());
  }

  clear() {
    this.form = this.builder.group({
      title: this.builder.control('', Validators.required),
      tasks: this.builder.array([this.buildItem()], Validators.required)
    });
  }

  async save() {
    try {
      const note: any = {
        title: this.form.get('title').value,
        tasks: this.form.get('tasks').value,
        userId: JSON.parse(sessionStorage.getItem('user'))._id
      };
      let response;

      if (this.note && this.note._id) {
        note._id = this.note._id;
        response = await this.service.update(note);
      } else {
        response = await this.service.insert(note);
      }

      let noteResponse = this.note && this.note._id ? note : response.data;
      this.form = this.builder.group({
        title: this.builder.control('', Validators.required),
        tasks: this.builder.array([this.buildItem()], Validators.required)
      });
      this.note = undefined;
      toast('Note saved successfully', 2000);
      this.saveEvent.emit(noteResponse);
    } catch (error) {
      toast(error.error, 4000);
    }
  }

  update(note: NoteModel) {
    const tasks = note.tasks.map(task => this.buildItem(task.description, task.completed));
    this.note = note;
    this.form = this.builder.group({
      title: this.builder.control(note.title, Validators.required),
      tasks: this.builder.array(tasks, Validators.required)
    });
  }
}
