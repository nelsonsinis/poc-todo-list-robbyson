export interface NoteModel {
    _id: String;
    userId: String;
    title: String;
    tasks: TaskModel[];
}

interface TaskModel {
    _id: String;
    completed: Boolean;
    description: String;
}