import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NoteModel } from './note.model';
import { NotesService } from '../notes.service';
import { NotesRepository } from '../notes.repository';
import { toast, MaterializeAction } from 'angular2-materialize';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'rb-note',
    templateUrl: './note.component.html',
    styleUrls: ['./note.component.css'],
    providers: [
        NotesService,
        NotesRepository
    ]
})
export class NoteComponent implements OnInit {
    @Input() note: NoteModel;
    @Output() update = new EventEmitter();
    @Output() delete = new EventEmitter<String>();

    noteForm: FormGroup;
    private service: NotesService;
    private builder: FormBuilder;
    modalActions = new EventEmitter<string | MaterializeAction>();

    constructor(service: NotesService, builder: FormBuilder) {
        this.service = service;
        this.builder = builder;

    }

    ngOnInit() {
        this.noteForm = this.builder.group({
            title: this.builder.control(this.note.title, Validators.required),
            tasks: this.builder.array(this.buildItems(), Validators.required)
        });
    }

    async updateNote(event, _id) {
        try {
            await this.service.update(this.note);
        } catch (error) {
            toast(error.error || 'An error occurred', 4000);
        }
    }

    async deleteNote() {
        try {
            await this.service.delete(this.note._id);
            this.delete.emit(this.note._id);
            toast('Note deleted successfully', 2000)
        } catch (error) {
            toast(error.error || 'An error occurred', 4000);
        }
    }

    buildItems() {
        return this.note.tasks.map(task => {
            return this.builder.group({
                description: this.builder.control(task.description, Validators.required),
                completed: this.builder.control(task.completed, Validators.required)
            });
        });
    }

    edit() {
        this.modalActions.emit({
            action: 'modal',
            params: [
                'open'
            ]
        })
        this.update.emit(this.note);
    }

    async updateTask() {
        try {
            this.note.tasks = this.noteForm.get('tasks').value;
            await this.service.update(this.note);
        } catch (error) {
            toast(error.error, 4000);
        }
    }
}