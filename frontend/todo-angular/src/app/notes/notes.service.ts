import { Injectable } from '@angular/core';
import { NotesRepository } from './notes.repository';
import { UserModel } from '../login/register/user.model';
import { NoteModel } from './note/note.model';

@Injectable()
export class NotesService {
    private repository: NotesRepository;

    constructor(repository: NotesRepository) {
        this.repository = repository;
    }

    list(): Promise<any> {
        const user: UserModel = JSON.parse(sessionStorage.getItem('user'));
        return this.repository.list(user._id).toPromise();
    }

    update(note: NoteModel): Promise<any> {
        return this.repository.update(note).toPromise();
    }

    insert(note: NoteModel): Promise<any> {
        return this.repository.insert(note).toPromise();
    }

    delete(_id): Promise<any> {
        return this.repository.delete(_id).toPromise();
    }
}