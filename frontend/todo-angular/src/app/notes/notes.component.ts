import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NoteModel } from './note/note.model';
import { toast } from 'angular2-materialize';
import { NotesService } from './notes.service';
import { NotesRepository } from './notes.repository';

@Component({
  selector: 'rb-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css'],
  providers: [
    NotesService,
    NotesRepository
  ]
})
export class NotesComponent implements OnInit {
  notes: NoteModel[];
  note: NoteModel;
  @Output() updateEvent = new EventEmitter();
  private service: NotesService;

  constructor(service: NotesService) {
    this.service = service;
  }

  async ngOnInit() {
    try {
      const response = await this.service.list();
      this.notes = response.items;
    } catch (error) {
      toast(error || 'An error occurred', 4000);
    }
  }

  addNote(event) {
    const index = this.notes.findIndex(note => note._id === event._id);

    if (index === -1) {
      this.notes.push(event);
    } else {
      this.notes.splice(index, 1, event);
    }
  }

  deleteNote(_id) {
    const index = this.notes.findIndex(note => note._id === _id);
    this.notes.splice(index, 1);
  }

  updateNote(note) {
    this.updateEvent.emit(note)
  }
}