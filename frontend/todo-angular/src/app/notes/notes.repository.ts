import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { NoteModel } from "./note/note.model";

@Injectable()
export class NotesRepository {
    private http: HttpClient;

    constructor(http: HttpClient) {
        this.http = http;
    }

    list(userId) {
        return this.http.get(environment.serviceNote, {
            params: {
                userId
            }
        });
    }

    update(note: NoteModel) {
        return this.http.put(environment.serviceNote, note);
    }

    insert(note: NoteModel) {
        return this.http.put(`${environment.serviceNote}/insert`, note);
    }

    delete(_id) {
        return this.http.delete(`${environment.serviceNote}/${_id}`);
    }
}