import { AppRepository } from "./app.repository";
import { Injectable } from "@angular/core";

@Injectable()
export class AppService {
    private repository: AppRepository;

    constructor(repository: AppRepository) {
        this.repository = repository;
    }

    validToken() {
        return new Promise((resolve, reject) => {
            this.repository.validToken().subscribe((data: any) => {
                resolve(data.data);
            }, error => {
                reject(error);
            });
        });
    }
}