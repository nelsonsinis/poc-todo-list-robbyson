import { Injectable, NgModule } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders, HTTP_INTERCEPTORS } from '@angular/common/http';

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler) {
        const token = sessionStorage.getItem('token');

        if (token) {
            const headers = new HttpHeaders({
                'x-access-token': token
            });

            req = req.clone({
                headers
            });
        }

        return next.handle(req);
    }
}

@NgModule({
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AppHttpInterceptor,
            multi: true
        }
    ]
})
export class Interceptor {}