import { Component, OnInit } from '@angular/core';
import { UserModel } from '../login/register/user.model';
import { AppService } from '../app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'rb-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user: UserModel;
  private router: Router;

  constructor(router: Router) {
    this.router = router;
  }

  ngOnInit() {
    if (sessionStorage.getItem('user')) {
      this.user = JSON.parse(sessionStorage.getItem('user'));
    }
  }

  showUserInfo() {
    return sessionStorage.getItem('user') ? true : false;
  }

  getRoute() {
    if (sessionStorage.getItem('token')) {
      return 'home';
    }

    return '';
  }

  exit() {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('user');
    this.router.navigate(['']);
  }
}
