import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl
} from '@angular/forms';
import { LoginService } from './login.service';
import { LoginRepository } from './login.repository';
import { Router } from '@angular/router';

@Component({
  selector: 'rb-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [
    LoginService,
    LoginRepository
  ]
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  errorMessages: string[];
  private formBuilder: FormBuilder;
  private service: LoginService;
  private router: Router;

  constructor(formBuilder: FormBuilder, service: LoginService, router: Router) {
    this.formBuilder = formBuilder;
    this.service = service;
    this.router = router;
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: this.formBuilder.control('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      password: this.formBuilder.control('', [
        Validators.required,
        Validators.minLength(6)
      ])
    });
  }

  validUsername() {
    const username = this.loginForm.get('username');
    return username.touched && username.dirty && username.invalid;
  }

  validPassword() {
    const password = this.loginForm.get('password');
    return password.touched && password.dirty && password.invalid;
  }

  async login() {
    const username = this.loginForm.get('username').value;
    const password = this.loginForm.get('password').value;

    try {
      const response: any = await this.service.login(username, password);
      const { user, token } = response.data;

      sessionStorage.setItem('token', token);
      sessionStorage.setItem('user', JSON.stringify(user));
      this.router.navigate(['home']);
    } catch (error) {
      this.errorMessages = error.error ? error.error.error : ['An error ocurred'];
    }
  }
}