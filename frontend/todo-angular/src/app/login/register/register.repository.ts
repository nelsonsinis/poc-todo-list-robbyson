import { HttpClient } from "@angular/common/http";
import { UserModel } from "./user.model";
import { environment } from '../../../environments/environment';
import { Injectable } from "@angular/core";

@Injectable()
export class RegisterRepository {
    private http: HttpClient;
    
    constructor(http: HttpClient) {
        this.http = http;
    }

    register(user: UserModel) {
        return this.http.post(environment.serviceUser, user);
    }
}