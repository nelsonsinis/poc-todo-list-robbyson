import { Injectable } from "@angular/core";
import { RegisterRepository } from "./register.repository";
import { UserModel } from "./user.model";

@Injectable()
export class RegisterService {
    private repository: RegisterRepository;

    constructor(repository: RegisterRepository) {
        this.repository = repository;
    }

    register(user: UserModel) {
        return new Promise((resolve, reject) => {
            this.repository.register(user).subscribe(data => {
                resolve(data);
            }, error => {
                reject(error);
            });
        });
    }
}