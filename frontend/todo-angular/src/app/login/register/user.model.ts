export interface UserModel {
    _id: string,
    username: string,
    phoneNumber: string,
    password: string,
    displayName: string,
    photo: string
}