import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegisterService } from './register.service';
import { UserModel } from './user.model';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';
import { LoginRepository } from '../login.repository';
import { RegisterRepository } from './register.repository';
import { toast } from 'angular2-materialize';

@Component({
  selector: 'rb-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [
    RegisterService,
    RegisterRepository,
    LoginService,
    LoginRepository
  ]
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  private formBuilder: FormBuilder;
  private service: RegisterService;
  private loginService;
  private router: Router;
  errors: string[] = [];

  constructor(formBuilder: FormBuilder, service: RegisterService, loginService: LoginService, router: Router) {
    this.formBuilder = formBuilder;
    this.service = service;
    this.loginService = loginService;
    this.router = router;
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: this.formBuilder.control('', [
        Validators.minLength(6),
        Validators.required
      ]),
      phoneNumber: this.formBuilder.control('', [
        Validators.minLength(12),
        Validators.required
      ]),
      password: this.formBuilder.control('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      displayName: this.formBuilder.control('', [
        Validators.required
      ]),
      photo: this.formBuilder.control('', [
        Validators.required
      ])
    });
  }

  onFileChange(event) {
    let reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;

      reader.readAsDataURL(file);

      reader.onload = () => {
        this.registerForm.patchValue({
          photo: reader.result
        });
      }
    }
  }

  register() {
    const user: UserModel = {
      _id: '',
      username: this.registerForm.get('username').value,
      phoneNumber: `+${this.registerForm.get('phoneNumber').value}`,
      password: this.registerForm.get('password').value,
      displayName: this.registerForm.get('displayName').value,
      photo: this.registerForm.get('photo').value
    };

    this.service.register(user).then(data => {
      this.showToast('User registered successfully');
      this.router.navigate(['']);
    }).catch(error => {
      this.showToast('An error occurred');
      this.errors = error;
    });
  }

  showToast(message) {
    toast(message, 4000);
  }
}
