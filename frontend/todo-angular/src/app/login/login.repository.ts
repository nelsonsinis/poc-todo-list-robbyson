import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable()
export class LoginRepository {
    private http: HttpClient;

    constructor(http: HttpClient) {
        this.http = http;
    }

    login(username, password)  {
        return this.http.post(environment.serviceUser, {
            username,
            password
        })
    }
}
