import { LoginRepository } from './login.repository';
import { Injectable } from '@angular/core';

@Injectable()
export class LoginService {
    private repository: LoginRepository;

    constructor(repository: LoginRepository) {
        this.repository = repository;
    }

    login(username: string, password: string) {
        return new Promise((resolve, reject) => {
            this.repository.login(username, password).subscribe((data: any) => {
                resolve(data);
            }, error => {
                reject(error);
            });
        });
    }
}