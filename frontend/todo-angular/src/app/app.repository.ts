import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../environments/environment";
import { Injectable } from "@angular/core";

@Injectable()
export class AppRepository {
    private http: HttpClient;

    constructor(http: HttpClient) {
        this.http = http;
    }

    validToken() {
        return this.http.get(environment.serviceUser);
    }
}