import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../app.service';
import { toast, MaterializeAction } from 'angular2-materialize';
import { UserModel } from '../login/register/user.model';
declare var $: any;
@Component({
  selector: 'rb-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  private router: Router;
  private appService: AppService;
  user: UserModel;
  modalActions = new EventEmitter<string|MaterializeAction>();
  test = false;

  constructor(router: Router, appService: AppService) {
    this.router = router;
    this.appService = appService;
  }

  async ngOnInit() {
    try {
      this.user = JSON.parse(sessionStorage.getItem('user'));
      await this.appService.validToken();
      $(document).ready(function(){
        $('.modal').modal();
      });
    } catch(error) {
      sessionStorage.removeItem('user');
      sessionStorage.removeItem('token');
      toast('Your session was expired', 4000);
      this.router.navigate(['']);
    }
  }
}
