import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import 'materialize-css'
import { MaterializeModule } from 'angular2-materialize';
import { ROUTES } from './app.routes';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './login/register/register.component';
import { AppService } from './app.service';
import { AppRepository } from './app.repository';
import { NoteComponent } from './notes/note/note.component';
import { Interceptor } from './app.interceptor';
import { NotesComponent } from './notes/notes.component';
import { ModalNoteComponent } from './notes/modal-note/modal-note.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    HomeComponent,
    RegisterComponent,
    NoteComponent,
    NotesComponent,
    ModalNoteComponent
  ],
  imports: [
    HttpClientModule,
    MaterializeModule,
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot(ROUTES, {
      preloadingStrategy: PreloadAllModules
    }),
    FormsModule,
    Interceptor
  ],
  providers: [
    AppService,
    AppRepository,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
