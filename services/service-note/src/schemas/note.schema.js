const Schema = require('mongoose').Schema;

module.exports = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    tasks: [
        {
            completed: {
                type: Boolean,
                required: true
            },
            description: {
                type: String,
                required: true
            }
        }
    ]
}, {
    collection: 'notes'
});