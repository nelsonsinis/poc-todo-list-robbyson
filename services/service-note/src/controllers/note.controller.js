const service = require('../services/note.service');

module.exports = {
    async insert(req, res) {
        const note = req.body;

        try {
            const response = await service.insert(note);
            res.status(200).json({
                data: response
            });
        } catch (error) {
            res.status(500).json(error);
        }
    },

    async list(req, res) {
        const userId = req.query.userId;

        try {
            const notes = await service.list(userId);
            res.status(200).json({
                items: notes,
                total: notes.length
            });
        } catch (error) {
            res.status(500).json(error);
        }
    },

    async get(req, res) {
        const _id = req.params._id;

        try {
            const note = await service.get(_id);
            res.status(200).json({
                data: note
            });
        } catch (error) {
            res.status(500).json(error);
        }
    },

    async delete(req, res) {
        const _id = req.params._id;

        try {
            await service.delete(_id);
            res.status(200).end();
        } catch(error) {
            res.status(500).json(error);
        }
    },

    async update(req, res) {
        try {
            const note = req.body;

            const response = await service.update(note._id, note);
            res.status(200).json({
                data: response
            });
        } catch(error) {
            res.status(500).json(error);
        }
    }
}