const repository = require('../repositories/note.repository');
const {
    Types
} = require('mongoose');
const Validator = require('../validator/note.validator');

module.exports = {
    insert(note) {
        const validator = new Validator(note);
        validator.onInserting();

        if (!validator.isValid()) {
            return Promise.reject(validator.messages);
        }

        note.userId = Types.ObjectId(note.userId);
        return repository.insert(note);
    },

    list(userId) {
        userId = Types.ObjectId(userId);
        return repository.list(userId);
    },

    get(_id) {
        return repository.get(Types.ObjectId(_id));
    },

    delete(_id) {
        return repository.delete(_id);
    },

    async update(_id, newNote) {
        try {
            const validator = new Validator(newNote);
            const savedNote = await repository.get(Types.ObjectId(_id));
            
            if (!savedNote) {
                return Promise.return(['Note not found in database']);
            }
            
            validator.onUpdating(savedNote);
            if (!validator.isValid()) {
                return Promise.reject(validator.messages);
            }
    
            return repository.update(_id, newNote);
        } catch(error) {
            return Promise.reject(error);
        }
    }
}