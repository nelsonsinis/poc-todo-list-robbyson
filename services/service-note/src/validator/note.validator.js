const { ObjectId } = require('mongoose').Types;

class Validator {
    constructor(note) {
        this._note = note;
        this._messages = [];
        // this.onInserting = this.onInserting.bind(this);
        // this.onUpdating = this.onUpdating.bind(this);
        // this.isValid = this.isValid.bind(this);
    }

    isValid() {
        return this._messages.length ? false : true;
    }

    get messages() {
        return this._messages;
    }

    onInserting() {
        if (!this._note.userId) {
            this._messages.push('No user id provided!');
        } else if (!ObjectId.isValid(this._note.userId)) {
            this._messages.push('User id provided isn\'t valid!');
        }
        
        if (!this._note.title) {
            this._messages.push('The note title is empty!');
        }
        
        if (!this._note.tasks.length) {
            this._messages.push('The tasks is empty!');
        }
    }
    
    onUpdating(savedNote) {
        if (!this._note.userId) {
            this._messages.push('No user id provided!');
        } else if (!ObjectId.isValid(this._note.userId)) {
            this._messages.push('User id provided isn\'t valid!');
        } else if (!ObjectId(this._note.userId).equals(savedNote.userId)) {
            this._messages.push('The supplied user id is different from id saved in the database!');
        }
    
        if (!this._note.title) {
            this._messages.push('The note title is empty!');
        }
    
        if (!this._note.tasks.length) {
            this._messages.push('The tasks is empty!');
        }
    }
}

module.exports = Validator;