const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const routes = require('../routes/note.routes');
const database = require('../lib/database/mongo');
const jwt = require('jsonwebtoken');
const cors = require('cors');

if (process.env.NODE_ENV === 'development') {
    require('dotenv').config({
        path: '../../../../'
    });
}

app.use(cors());

app.use((req, res, next) => {
    const token = req.headers['x-access-token'];
    
    jwt.verify(token, process.env.JWT_TOKEN, (error) => {
        if (error) {
            res.status(500).json(error);
            return;
        }
        
        next();
    });
});

app.use(bodyParser.json());
app.use('/', routes);

app.set('port', 3002);
database.connect();

module.exports = app;