const router = require('express').Router();
const controller = require('../controllers/note.controller');

router.put('/insert', controller.insert);
router.get('/', controller.list);
router.get('/:_id', controller.get);
router.delete('/:_id', controller.delete);
router.put('/', controller.update);

module.exports = router;