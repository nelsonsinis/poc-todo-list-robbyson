const mongoose = require('mongoose');
const schema = require('../schemas/note.schema');

module.exports = {
    insert(note) {
        const model = mongoose.model('notes', schema);
        return model.create(note);
    },

    list(userId) {
        const model = mongoose.model('notes', schema);
        return model.find({
            userId
        });
    },

    get(_id) {
        const model = mongoose.model('notes', schema);
        return model.findById(_id);
    },

    delete(_id) {
        const model = mongoose.model('notes', schema);
        return model.deleteOne({_id});
    },

    update(_id, newNote) {
        const model = mongoose.model('notes', schema);
        return model.updateOne({_id}, newNote);
    }
}