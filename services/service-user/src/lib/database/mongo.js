const mongoose = require('mongoose');

module.exports = {
    connect() {
        mongoose.connect(`mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@${process.env.API_HOST}:27017/users?authSource=admin`, error => {
            if (error) {
                throw new Error(error);
            }

            console.log('Mongo connected successfully');
        });
    }
}