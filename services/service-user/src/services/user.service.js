const repository = require('../repositories/user.repository');
const Validator = require('../validators/user.validator');
const sha1 = require('sha1');
const jwt = require('jsonwebtoken');

module.exports = {
    login(username, password) {
        password = sha1X2(password);
        const validator = new Validator();

        return new Promise(async (resolve, reject) => {
            try {
                const user = await repository.find(username);
                if (!user) {
                    reject(['This user is not registered!']);
                }

                validator.validLogin({
                    username,
                    password
                }, user);

                if (!validator.valid()) {
                    reject(validator.messages);
                }

                const token = jwt.sign({
                    username,
                    _id: user._id,
                    date: new Date().toISOString()
                }, process.env.JWT_TOKEN, {
                    expiresIn: '6h'
                });

                resolve({
                    user: {
                        _id: user._id,
                        username: user.username,
                        phoneNumber: user.phoneNumber,
                        displayName: user.displayName,
                        photo: user.photo,
                    },
                    token
                });
            } catch (error) {
                reject(error);
            }
        });
    },

    async register(newUser) {
        try {
            const validator = new Validator();
            validator.validUser(newUser);
            
            if (!validator.valid()) {
                return Promise.reject(validator.messages)
            }
            const user = await repository.find(newUser.username);

            if (user) {
                return Promise.reject(['User already registered!']);
            }
    
            newUser.password = sha1X2(newUser.password);
            return repository.register(newUser);
        } catch(error) {
            return Promise.reject(error);
        }
    },

    validToken(token) {
        return new Promise((resolve, reject) => {
            jwt.verify(token, process.env.JWT_TOKEN, (error, decoded) => {
                if (error) {
                    reject(error)
                }

                resolve(true);
            });
        })
    }
}

function sha1X2(pass) {
    let hash = pass;

    for (let i = 0; i < 2; i++) {
        hash = sha1(hash);
    }

    return hash;
}