const mongoose = require('mongoose');
const schema = require('../schemas/user.schema');

module.exports = {
    register(user) {
        const model = mongoose.model('user', schema);
        return model.create(user);
    },
    find(username) {
        const model = mongoose.model('user', schema);
        return model.findOne({
            username
        });
    }
}