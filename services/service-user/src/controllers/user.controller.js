const service = require('../services/user.service');

module.exports = {
    async login(req, res) {
        const {
            username,
            password
        } = req.body;

        try {
            const response = await service.login(username, password);

            res.status(200).json({
                data: response
            });
        } catch (error) {
            res.status(500).json({
                error
            });
        }
    },

    async register(req, res) {
        const user = req.body;

        try {
            const response = await service.register(user);
            res.status(200).json({
                data: response
            });
        } catch (error) {
            res.status(500).json({
                error
            });
        }
    },

    async validToken(req, res) {
        const token = req.headers['x-access-token'];

        try {
            const response = await service.validToken(token);
            res.status(200).json({
                data: response
            });
        } catch (error) {
            res.status(500).json({
                error
            });
        }
    }
}