const router = require('express').Router();
const controller = require('../controllers/user.controller');

router.post('/', controller.login);
router.post('/register', controller.register);
router.get('/', controller.validToken);

module.exports = router;