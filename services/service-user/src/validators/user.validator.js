module.exports = class Validator {
    constructor() {
        this._messages = [];
    }

    validUser(user) {
        if (!user.username) {
            this._messages.push('The username cannot is empty!');
        }

        if (!user.password) {
            this._messages.push('The passoword cannot is empty!');
        } else if(user.password.length < 6) {
            this._messages.push('The password must be longer than 6 characters!');
        }

        if (!user.phoneNumber) {
            this._messages.push('The phone number cannot is empty');
        } else if (!user.phoneNumber.match(/\d+/)) {
            this._messages.push('The phone number needs to be just numbers');
        }

        if (!user.displayName) {
            this._messages.push('The display name cannot is empty');
        }

        if (!user.photo) {
            this._messages.push('The photo cannot is empty');
        }
    }

    validLogin(userLogin, {username, password}) {
        if (!userLogin.username) {
            this._messages.push('The username cannot is empty!');
        } else if (userLogin.username.toUpperCase() !== username.toUpperCase()) {
            this._messages.push('The username is wrong!')
        }

        if (!userLogin.password) {
            this._messages.push('The password cannot is empty!');
        } else if (userLogin.password.toUpperCase() !== password.toUpperCase()) {
            this._messages.push('The password is wrong!');
        }
    }

    valid() {
        return this._messages.length ? false : true;
    }

    get messages() {
        return this._messages;
    }
}