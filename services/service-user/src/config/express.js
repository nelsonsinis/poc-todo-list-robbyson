const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const database = require('../lib/database/mongo');
const cors = require('cors');

if (process.env.NODE_ENV === 'development') {
    require('dotenv').config({
        path: '../../../../'
    });
}

const routes = require('../routes/user.routes');

app.set('port', 3001);

app.use(cors());

app.use(bodyParser.json());
database.connect();

app.use('/', routes);

module.exports = app;