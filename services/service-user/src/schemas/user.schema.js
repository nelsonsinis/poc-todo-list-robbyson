const Schema = require('mongoose').Schema;

module.exports = new Schema({
    displayName: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    phoneNumber: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    photo: {
        type: String,
        required: true
    }
}, {
    collection: 'users'
});